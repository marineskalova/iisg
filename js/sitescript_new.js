// begin from sitescript
$(document).ready(function(){	
    function copyToClipboard(element) {
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
    }
	$('.copyitem').click(function(e) { 
		  copyToClipboard('.card-info__number');
    });			
});
// end from sitescript

// begin from home
jQuery(function() {
  initAddClasses();
});


// add class on click
function initAddClasses() {
  jQuery('a.link-holder__link').clickClass({
    classAdd: 'form-password',
    addToParent: 'user-login'
  });
}


/*
 * Add class plugin
 */
jQuery.fn.clickClass = function(opt) {
  var options = jQuery.extend({
    classAdd: 'add-class',
    addToParent: false,
    event: 'click'
  }, opt);

  return this.each(function() {
    var classItem = jQuery(this);
    if(options.addToParent) {
      if(typeof options.addToParent === 'boolean') {
        classItem = classItem.parent();
      } else {
        classItem = classItem.parents('.' + options.addToParent);
      }
    }
    jQuery(this).bind(options.event, function(e) {
      classItem.toggleClass(options.classAdd);
      e.preventDefault();
    });
  });
};

// slick

$(function(){

  if($('.slick-slider').length) {
    $('.slick-slider').slick({
      slidesToScroll: 1,
      prevArrow: '<button class="slick-prev">Previous</button>',
      nextArrow: '<button class="slick-next">Next</button>',
      variableWidth: true
    });
  };
  
});

// открытие видео

$(function() {

  $("#video__play").click(function() {
      var dataYoutube = $(this).parents('.js-video').attr('data-youtube');
      $(this).parents('.js-video').html('<iframe src="https://www.youtube.com/embed/'+ dataYoutube +'?autoplay=1" frameborder="0" allowfullscreen></iframe>')
  });

});
// begin from home

// begin from registration
$(document).ready(function(){
// ====================================================== //


    function validateRegExp(regExp) {
        return function(value) {
            return regExp.test(value)
        }
    }

    function validateRequired(value) {
        return !!value;
    }

    function validateElement(data, target) {
        if (!data || !target) return false;

        var name = target.name;
        var value = target.value;
        var isValid = !data[name].some(function (el) {return el(value) === false;});

        if (isValid) {
                $(target).removeClass('invalid');
                $(target).prev('label').removeClass('invalid');
            } else {
                $(target).addClass('invalid');
                $(target).prev('label').addClass('invalid');
            }

        if($('input, label').hasClass('invalid')){         
                $('.message-error').slideDown(); 
                $('.button-send').addClass('invalid')
            }else{
                $('.message-error').slideUp();
                $('.button-send').removeClass('invalid');
            }



        return isValid
    }

    function forceValidate(data, selector) {
        var key,
            value, 
            isValid = true;

        for (key in data) {
            if (!validateElement(data, $('[name=' + key + ']' + selector)[0])) {
                isValid = false;
            }
        }

        return isValid;
    }

    var formData = {
        supervisor: [
            validateRequired
        ],
        curator: [
            validateRequired
        ],
        firstName: [
            validateRegExp(/^([а-я]|[А-Я]|[a-z]|[A-z]){2,40}$/)
        ],
        lastName: [
            validateRegExp(/^([а-я]|[А-Я]|[a-z]|[A-z]){2,40}$/)
        ],
        city: [
            validateRegExp(/^([а-я]|[А-Я]|[a-z]|[A-z]){2,40}$/)
        ],
        password: [
            validateRequired
        ],
        passwordAdain: [
            validateRequired
        ],
        email: [
            validateRegExp(/^.+@.+[.].{2,}$/i)
        ],
        tel: [
            validateRegExp(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/)
        ]
    }

    var FORM_SELECTOR = '.form-validation'

    $(FORM_SELECTOR).blur(function(event) {
        validateElement(formData, event.target)
    })

    $('.button-send').click(function (event) {
        event.preventDefault();
        forceValidate(formData, FORM_SELECTOR)

        if($('input, label').hasClass('invalid')){    
            $.fancybox.close();
            }else{
                $.fancybox.open({
                    src  : '#command-register',
                    type : 'inline',
                    opts : {
                        onComplete : function() {
                        }
                    }
                });     
            }
        return false;
    })

});


 
