$(document).ready(function(){	
    
    
    $('.menu__item_parent .menu__link').click(function(e) {
					e.preventDefault();
					  $(this).next('.submenu').slideToggle();
       $(this).parent('.menu__item_parent').toggleClass('menu__item_opened, menu__item_active');
								
     });
    
   
         var highestBox = 0;      
      $('.eq-height', this).each(function(){        
        if($(this).innerHeight() > highestBox) {
          highestBox = $(this).innerHeight(); 
        }
      });
      $('.eq-height',this).css('min-height',highestBox);
    
    
    
    $('#container').bind('heightChange', function(){
        var highestBox = 0;      
          $('.eq-height', this).each(function(){  
            $(this).css('min-height','initial');
            if($(this).innerHeight() > highestBox) {
              highestBox = $(this).innerHeight(); 
            }
          });
          $('.eq-height',this).css('min-height',highestBox);
    });
    
    
    
    /*block-version equial height*/
     var highestBox = 0;      
      $('.block-version-info', this).each(function(){        
        if($(this).innerHeight() > highestBox) {
          highestBox = $(this).innerHeight(); 
        }
      });
     $('.block-version-info',this).css('height',highestBox);
    /*block-version equial height*/
    
    /*fancybox
    $("[data-fancybox]").fancybox({
		  // Options will go here
	    });*/
    
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
				$('.copyitem').click(function(e) { 
					  copyToClipboard('.card-info__number');
     });
				
				
    
    $('.td-show-more').click(function(e) {        
        
        $(this).parents('tr').toggleClass('tr-opened');
        
        var order=$(this).attr('data-order');
        $('.sub-level[data-order="'+order+'"]').toggleClass('hidden');
        
        $("#container").trigger('heightChange');
        return false;
     });
					
					$('.status-info').click(function(e) { 
					  $(this).next('.status-info-more').slideToggle();
     });

    /* custom-input - iCheck*/
     $('.custom-input').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal',
        increaseArea: '20%' // optional
      });

    $('input[type="radio"]').on('ifChecked', function(event){
    	var self = $(this);

    	self.parents('.choose-product__item')
    		.addClass('choose-product__item--border-blue');

    	self.parent()
    		.siblings()
    		.addClass('text-dark');

    	self.parents('.tariff-radio')
    		.siblings()
    		.addClass('tariff-radio-info--text-dark');
	});

	$('input[type="radio"]').on('ifUnchecked', function(event){
		var self = $(this);

		self.parents('.choose-product__item')
			.removeClass('choose-product__item--border-blue');

		self.parent()
			.siblings()
			.removeClass('text-dark');

		self.parents('.tariff-radio')
			.siblings()
			.removeClass('tariff-radio-info--text-dark');
	});
	/* custom-input - iCheck*/


$('.support-topic-short').click(function() {
        $(this).parent().siblings().find('.support-topic-messages').slideUp();
								$(this).parent().siblings().find('.support-topic-short').removeClass('active');
								$(this).toggleClass('active').next().slideToggle();
				
	 setTimeout(
  function() 
  {
    $('html, body').animate({
        scrollTop: $('.support-topic-short.active').offset().top
    }, 500); 
  }, 500);
    
});
    

	$('.file').change(function(){
		if( $(this).val().length < 35 ) {
			$('#filetext').html($(this).val());
		} else {
			$('#filetext').html($(this).val().substring(0,25) + '...');
		}
		
		});
		if ($('.phone').length){
			$('.phone').mask("+7 (999) 999-99-99");
		}
		
		$(".select").each(function() {					
					var sb = new SelectBox({
						selectbox: $(this),
                        width:'100%', 
                        height:320
					});
				});
				

if ($('.graph-general').length){
	
	Highcharts.setOptions({
            lang: {
                loading: 'Загрузка...',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                exportButtonTitle: "Экспорт",
                printButtonTitle: "Печать",
                rangeSelectorFrom: "С",
                rangeSelectorTo: "По",
                rangeSelectorZoom: "Период",
                downloadPNG: 'Скачать PNG',
                downloadJPEG: 'Скачать JPEG',
                downloadPDF: 'Скачать PDF',
                downloadSVG: 'Скачать SVG',
                printChart: 'Напечатать график'
            },
												
												chart: {
        type: 'line',
        backgroundColor: '#ffffff',
        plotBackgroundColor: '#f8f9fa',
								spacingRight: 0, 
								spacingLeft: 0
    },
    title:{  text: '' },
    subtitle: {  text: '' },
    exporting: { enabled: false },
    credits: { enabled: false },
    xAxis: {
        type: 'datetime',
							 gridLineWidth: 1,
								minPadding: 0,
        labels: {
          formatter: function() {
            return Highcharts.dateFormat("%B'%y", this.value);
          },
										align: 'left',
          min: 0,
									 offset: 0
        },
							tickColor: '#e5e9ee',
								minTickInterval: 3600*24*30*1000,//time in milliseconds
        minRange: 3600*24*30*1000,
        ordinal: false //this sets the fixed time formats  
    },
    yAxis: {
         title: {
		       text: ''
		     },
         min: 0,
								offset: 0,
        lineWidth: 1
    },
        legend: {
            align: 'left',
												x: 45
        },
    tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
        pointFormat: '{point.x:%e  %b}: {point.y:.2f} '
    },

    plotOptions: {
        line: {
            marker: {
                enabled: false
            },
            color: '#008dcd'
        }
    }
    });

		
				
    Highcharts.chart('graph01', {
					 plotOptions: {
        line: {
            color: '#bb2433'
        }
      },
      series: [{
        name: 'Общее количество участников',
        data: [
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
				
				Highcharts.chart('graph02', {
      series: [{
        name: 'Количество новых участников',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
				
				Highcharts.chart('graph03', {
      series: [{
        name: 'Общее количество',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
				
				Highcharts.chart('graph04', {
      series: [{
        name: 'Количество открытых циклов',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
				
				Highcharts.chart('graph05', {
      series: [{
        name: 'Количество закрытых циклов',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
	});
}
	
	

	$.each($('.tooltip'), function(index, element) {
        $(element).tooltipster({
            functionPosition: function(instance, helper, data) {    
                var gridBcr = element.getBoundingClientRect(),
                    arrowSize = parseInt($(helper.tooltipClone).find('.tooltipster-box').css('margin-left'));
                
                // override these
                data.coord = {
                    // move the tooltip so the arrow overflows the grid
                    left: gridBcr.left + 13,
                    top: gridBcr.top -32
                };
                
                return data;
            },
            contentCloning: true,
            maxWidth: 615,
            side: ['right'], 
            trigger: 'click'
         })
        
    })
        

    $('.command-register-continue').click(function(e) {
         $.fancybox.close();
        $.fancybox.open({
            src  : '#command-register-continue',
            type : 'inline',
            opts : {
                onComplete : function() {
                }
            }
        });
        return false;
     });
    
    $('.command-levels-item__link').click(function(e) {
        $('.command-levels-item').removeClass('command-levels-item_active');
        $(this).parent('.command-levels-item').addClass('command-levels-item_active');
        var href=$(this).attr('href');      
        $('.command-tabs__item').removeClass('active');
        $(href).addClass('active');
        return false;
     });
					
					

(function(a){if(typeof define==="function"&&define.amd){define(["jquery"],a)}else{a(jQuery)}}(function(d){var c="ellipsis",b='<span style="white-space: nowrap;">',e={lines:"auto",ellipClass:"ellip",responsive:false};function a(h,q){var m=this,w=0,g=[],k,p,i,f,j,n,s;m.$cont=d(h);m.opts=d.extend({},e,q);function o(){m.text=m.$cont.text();m.opts.ellipLineClass=m.opts.ellipClass+"-line";m.$el=d('<span class="'+m.opts.ellipClass+'" />');m.$el.text(m.text);m.$cont.empty().append(m.$el);t()}function t(){if(typeof m.opts.lines==="number"&&m.opts.lines<2){m.$el.addClass(m.opts.ellipLineClass);return}n=m.$cont.height();if(m.opts.lines==="auto"&&m.$el.prop("scrollHeight")<=n){return}if(!k){return}s=d.trim(m.text).split(/\s+/);m.$el.html(b+s.join("</span> "+b)+"</span>");m.$el.find("span").each(k);if(p!=null){u(p)}}function u(x){s[x]='<span class="'+m.opts.ellipLineClass+'">'+s[x];s.push("</span>");m.$el.html(s.join(" "))}if(m.opts.lines==="auto"){var r=function(y,A){var x=d(A),z=x.position().top;j=j||x.height();if(z===f){g[w].push(x)}else{f=z;w+=1;g[w]=[x]}if(z+j>n){p=y-g[w-1].length;return false}};k=r}if(typeof m.opts.lines==="number"&&m.opts.lines>1){var l=function(y,A){var x=d(A),z=x.position().top;if(z!==f){f=z;w+=1}if(w===m.opts.lines){p=y;return false}};k=l}if(m.opts.responsive){var v=function(){g=[];w=0;f=null;p=null;m.$el.html(m.text);clearTimeout(i);i=setTimeout(t,100)};d(window).on("resize."+c,v)}o()}d.fn[c]=function(f){return this.each(function(){try{d(this).data(c,(new a(this,f)))}catch(g){if(window.console){console.error(c+": "+g)}}})}}));

if ($('.block-news__info').length){
$('.block-news__info p a').ellipsis({ lines: 3 });
}

});



 
